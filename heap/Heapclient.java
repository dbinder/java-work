package heap;

public class Heapclient
{
  public static void main(String[] args)
  {
    Heap<Integer> first = new Heap<Integer>();

    System.out.print("size = ");
    System.out.println(first.Size());
    first.Enqueue(5);
    System.out.println(first.top());
    System.out.print("size = ");
    System.out.println(first.Size());
    first.Enqueue(3);
    System.out.println(first.top());
    System.out.print("size = ");
    System.out.println(first.Size());
    first.Enqueue(7);
    System.out.println(first.top());
    System.out.print("size = ");
    System.out.println(first.Size());
    first.Enqueue(2);
    System.out.println(first.top());
    System.out.print("size = ");
    System.out.println(first.Size());
    first.Enqueue(1);
    System.out.println(first.top());
    System.out.print("size = ");
    System.out.println(first.Size());
    first.Enqueue(10);
    System.out.println(first.top());
    System.out.print("size = ");
    first.Enqueue(4);
    first.Enqueue(15);
    first.Enqueue(18);
    first.Enqueue(20);
    first.Enqueue(40);
    first.Enqueue(6);
    System.out.println(first.Size());
    System.out.println(first.Dequeue());
    System.out.println(first.Dequeue());
    System.out.println(first.Dequeue());
    System.out.println(first.Dequeue());
    System.out.println(first.Dequeue());
    System.out.println(first.Dequeue());
    System.out.println(first.Dequeue());
    System.out.println(first.Dequeue());
    System.out.println(first.Dequeue());
    System.out.println(first.Dequeue());
    System.out.println(first.Dequeue());
    System.out.println(first.Dequeue());
    System.out.println(first.Dequeue());
    System.out.println(first.Dequeue());
    System.out.println(first.Dequeue());
  }
}
