package heap;

public class Heap<E extends Comparable>
{
  private int m_size;
  private Object m_data[];
  private int m_index;
  private int m_counter;

  private class Node
  {
    public E m_data;
    public Node(E m_data)
    {
      this.m_data = m_data;
    }
  }

  public Heap()
  {
    m_data = new Object[64];
    m_index = 0;
    m_size = 0;
    m_counter = 0;
  }

  private int Left(int i)
  {
    return 2 * i + 1;
  }

  private int Right(int i)
  {
    return 2 * i + 2;
  }

  private int Parent(int i)
  {
    return (i - 1)/ 2;
  }

  public int Size()
  {
    return m_size;
  }

  public void Enqueue(E elt)
  {

    if(m_size == m_data.length )
    {
      biggerArray(m_size);
      m_size++;
      m_index++;
      Node insert = new Node(elt);
      m_data[m_index - 1] = insert;
      SiftUp(m_index - 1);
    }
    else
    {
      m_size++;
      m_index++;
      Node insert = new Node(elt);
      m_data[m_index - 1] = insert;
      SiftUp(m_index - 1);//helper function to correct the heap after insertion
    }
  }

  private void SiftUp(int index)
  {
    Node insert, parent, temp;
  //put in at m_index
  //if parent is less than swap else done or if elt reaches root, or m_index 0;
    if(index != 0)
    {
      insert = (Node)m_data[index];
      parent = (Node)m_data[Parent(index)];
      if(insert == parent)// copy last "node" to insert and sift down?
      {
        m_counter++;
        return;
      }

      if(insert.m_data.compareTo(parent.m_data) < 0)
      {
        temp = parent;
        m_data[Parent(index)] = insert;
        m_data[index] = temp;
        SiftUp(Parent(index));
      }
    }
  }

  public E Dequeue()
  {
    if(m_data[0] == null)
    {
      return null;
    }
    if(m_size == 1)
    {
      Node temp = (Node)m_data[0];
      m_data[0] = null;
      m_size--;
      m_index--;
      return temp.m_data;
    }
    else
    {
        Node temp = (Node)m_data[0];
        if(m_size > 0)
        {
          m_data[0] = m_data[m_size - 1];
          //m_data[size] = null;
          m_size--;
          m_index--;
          siftDown(0);
          //m_counter--;
          return temp.m_data;
        }
        else return null;
    }
  }
//compare children smaller swap with parent and repeat
//children are null done
  private void siftDown(int nodem_index)
  {
    Node parent, childleft, childright, temp;
    parent = (Node) m_data[nodem_index];
    childleft = (Node)m_data[Left(nodem_index)];
    childright = (Node)m_data[Right(nodem_index)];
    if(childleft == null && childright== null)
    {
      m_data[nodem_index]= parent;
    }
    else if(childright == null)
    {
      if(parent.m_data.compareTo(childleft.m_data) > 0)
      {
        temp = parent;
        m_data[nodem_index] = childleft;
        m_data[Left(nodem_index)]= temp;
      }else
      {
        if(parent == childleft && m_counter == 0 )
        m_data[Left(nodem_index)] = null;
      }
    }
    else if(childleft.m_data.compareTo(childright.m_data) <= 0)
    {
      if(parent.m_data.compareTo(childleft.m_data) > 0)
      {
        temp = parent;
        m_data[nodem_index] = childleft;
        m_data[Left(nodem_index)] = temp;
        siftDown(Left(nodem_index));
      }
      else if(parent == childleft && m_counter == 0)
      {
        m_data[Left(nodem_index)] = m_data[Right(nodem_index)];
        siftDown(Left(nodem_index));
      }
    }
    else if(childright.m_data.compareTo(childleft.m_data) < 0)
    {
      if(parent.m_data.compareTo(childright.m_data) > 0)
      {
        temp = parent;
        m_data[nodem_index] = childright;
        m_data[Right(nodem_index)] = temp;
        siftDown(Right(nodem_index));
      }
      else if(parent == childright && m_counter == 0 )
      {
        m_data[Right(nodem_index)] = null;
      }
      //if(parent == childright)
    }
  }
  //m_data[right(nodem_index)]= null;
  public E top ()    //m_index 0 node in there need to get to m_data
  {
    if(m_size  == 0)
    {
      return null;
    }
    else
    {
      Node temp = (Node) m_data[0];
      return temp.m_data;
    }
  }

  public void biggerArray(int count) //make a new array and copy
  {
    Object[] arraytwo = new Object [count + 1];
    for(int i =0; i < count; i++)
    {
      arraytwo[i] = m_data[i];
    }
    m_data = arraytwo;
  }
}
