package heap;
import junit.framework.*;

public class TestHeap extends TestCase
{
  public Integer i1 = new Integer(1);
  public Integer i2 = new Integer(2);
  public Integer i5 = new Integer(5);
  public Integer i7 = new Integer(7);
  public Integer i10 = new Integer(10);
  public Integer i15 = new Integer(15);
  public Integer i20 = new Integer(20);
  public Integer i25 = new Integer(25);

  public TestHeap(String name)
  {
    super(name);
  }

  public void TestEmpty()
  {
    Heap<Integer> foo = new Heap<Integer>();

    assertEquals("Empty test 0.1",0,foo.Size());
    assertEquals("Top Test ",null,foo.top());
    assertEquals("Dequeue Test 0",null,foo.Dequeue());
  }

  public void TestSize()
  {
    Heap<Integer> foo = new Heap<Integer>();
    assertEquals("Empty test 0.1",0,foo.Size());

    foo.Enqueue(i1);
    assertEquals("size test 0.1",1,foo.Size());
    foo.Enqueue(i2);
    assertEquals("size test 0.2",2,foo.Size());
    foo.Enqueue(i5);
    assertEquals("size test 0.3",3,foo.Size());
    foo.Enqueue(i10);
    assertEquals("size test 0.4",4,foo.Size());
    foo.Enqueue(i7);
    assertEquals("size test 0.5",5,foo.Size());
    assertEquals("Top test 0.1",i1,foo.top());

    foo.Enqueue(i2);
    assertEquals("Size test 0.6",6,foo.Size());
    assertEquals("Top Test  0.2",i1,foo.top());
  }


  public void TestDequeue()
  {
    Heap<Integer> foo = new Heap<Integer>();
    foo.Enqueue(i20);
    foo.Enqueue(i10);
    foo.Enqueue(i1);
    foo.Enqueue(i2);
    foo.Enqueue(i25);
    foo.Enqueue(i7);
    foo.Enqueue(i5);
    foo.Enqueue(i5);
    foo.Enqueue(i15);
    assertEquals("Size test 0.6",9,foo.Size());
    assertEquals("Top Test 0.2",i1,foo.top());

    assertEquals("Dequeue Test 1",i1,foo.Dequeue());
    assertEquals("Dequeue Test 2",i2,foo.Dequeue());
    assertEquals("Dequeue Test 3",i5,foo.Dequeue());
    assertEquals("Dequeue Test 4",i5,foo.Dequeue());
    assertEquals("Dequeue Test 5",i7,foo.Dequeue());
    assertEquals("Top Test 0.3",i10,foo.top());
    assertEquals("Dequeue Test 5",i10,foo.Dequeue());
    assertEquals("Dequeue Test 6",i15,foo.Dequeue());
    assertEquals("Dequeue Test 7",i20,foo.Dequeue());
    assertEquals("Dequeue Test 8",i25,foo.Dequeue());
    assertEquals("Dequeue Test 9",null,foo.Dequeue());
  }
}


