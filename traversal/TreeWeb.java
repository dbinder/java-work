package traversal;

public class TreeWeb {

		private class Node {
			public int key;
			public Node left;
			public Node right;

			public Node(int key, Node left, Node right){
				this.key = key;
				this.left = left;
				this.right = right;
			}

			public Node(int key){
				this.key = key;
				this.left = null;
				this.right = null;
			}
		}

		private Node root;
		private int size;

		public void Tree(){
			root = null;
			size = 0;
		}

		public int size() {
			return size;
		}

		public void add(int i){
			addAux(i, null, root);
		}

		private void addAux(int i, Node parent, Node node){
			if(i!=0){
				if(parent == null && node == null){//empty tree
					root = new Node(i);
					size++;
					return;
				}
				if(node == null){//empty node, yay
					Node tmp = new Node(i);
					if(i < parent.key)
						parent.left = tmp;
					else
						parent.right = tmp;
					size++;
					return;
				}
				//identical key
				if(i == node.key){
					return;
				}
				//keep going further
				if(i < node.key)
					addAux(i, node, node.left);
				else
					addAux(i, node, node.right);
			}
		}

		public Iterator mkBFSIterator() {
			return new BFSIterator();
		}

		public Iterator mkDFSIterator() {
			return new DFSIterator();
		}

		public Iterator mkPreorderIterator() {
			return new PreorderIterator();
		}

		public Iterator mkInorderIterator() {
			return new InorderIterator();
		}

		public Iterator mkPostorderIterator() {
			return new PostorderIterator();
		}

		public Iterator mkFrontierIterator() {
			return new FrontierIterator();
		}

		public class BFSIterator implements Iterator{

			private Node cursor;
			private Queue<Node> BFSQueue;
			public BFSIterator() {
				cursor = root;
				BFSQueue = new Queue<Node>();
			}

			public int get(){
				if(cursor != null)
					return cursor.key;
				else
					return 0;
			}

			public void next(){
				if(this.isValid()){
					if(cursor.left != null)
						BFSQueue.enqueue(cursor.left);
					if(cursor.right != null)
						BFSQueue.enqueue(cursor.right);

					if(BFSQueue.front() == null)
						cursor = null;
					else
						cursor = BFSQueue.dequeue();
				}
			}

			public boolean isValid(){
				if(cursor != null)
					return true;
				else
					return false;
			}
		}

		public class DFSIterator implements Iterator{

			private Node cursor;
			private Queue<Node> DFSQ;
			public DFSIterator() {
				DFSQ = new Queue<Node>();
				if(size > 0)
					auxDFS(root);
				cursor = DFSQ.dequeue();
			}

			private void auxDFS(Node curr){
				DFSQ.enqueue(curr);
				if(curr.left != null)
					this.auxDFS(curr.left);
				if(curr.right != null)
					this.auxDFS(curr.right);
			}
			public int get(){
				if(this.isValid())
					return cursor.key;
				else
					return 0;				
			}

			public void next(){
				if(this.isValid()){
					if(DFSQ.front() != null)
						cursor = DFSQ.dequeue();
					else
						cursor = null;
				}

			}

			public boolean isValid(){
				if(cursor != null)
					return true;
				else
					return false;
			}
		}

		public class PreorderIterator implements Iterator{

			private Node cursor;
			private Queue<Node> PreorderQ;
			public PreorderIterator() {
				PreorderQ = new Queue<Node>();
				if(size > 0)
					auxPreorder(root);
				cursor = PreorderQ.dequeue();
			}

			private void auxPreorder(Node curr){
				PreorderQ.enqueue(curr);
				if(curr.left != null)
					this.auxPreorder(curr.left);
				if(curr.right != null)
					this.auxPreorder(curr.right);
			}
			public int get(){
				if(this.isValid())
					return cursor.key;
				else
					return 0;				
			}

			public void next(){
				if(this.isValid()){
					if(PreorderQ.front() != null)
						cursor = PreorderQ.dequeue();
					else
						cursor = null;
				}

			}

			public boolean isValid(){
				if(cursor != null)
					return true;
				else
					return false;
			}
		}

		public class InorderIterator implements Iterator{
			private Node cursor;
			private Queue<Node> InorderQ;
			public InorderIterator() {
				InorderQ = new Queue<Node>();
				if(size > 0)
					auxInorder(root);
				cursor = InorderQ.dequeue();
			}

			private void auxInorder(Node curr){
				if(curr.left != null)
					this.auxInorder(curr.left);
				InorderQ.enqueue(curr);
				if(curr.right != null)
					this.auxInorder(curr.right);
			}
			public int get(){
				if(this.isValid())
					return cursor.key;
				else
					return 0;				
			}

			public void next(){
				if(this.isValid()){
					if(InorderQ.front() != null)
						cursor = InorderQ.dequeue();
					else
						cursor = null;
				}

			}

			public boolean isValid(){
				if(cursor != null)
					return true;
				else
					return false;
			}
		}

		public class PostorderIterator implements Iterator{
			private Node cursor;
			private Queue<Node> PostorderQ;
			public PostorderIterator() {
				PostorderQ = new Queue<Node>();
				if(size > 0)
					auxPostorder(root);
				cursor = PostorderQ.dequeue();
			}

			private void auxPostorder(Node curr){
				if(curr.left != null)
					this.auxPostorder(curr.left);
				if(curr.right != null)
					this.auxPostorder(curr.right);
				PostorderQ.enqueue(curr);
			}
			public int get(){
				if(this.isValid())
					return cursor.key;
				else
					return 0;				
			}

			public void next(){
				if(this.isValid()){
					if(PostorderQ.front() != null)
						cursor = PostorderQ.dequeue();
					else
						cursor = null;
				}

			}

			public boolean isValid(){
				if(cursor != null)
					return true;
				else
					return false;
			}
		}

		public class FrontierIterator implements Iterator{
			private Node cursor;
			private Queue<Node> LeafQ;
			public FrontierIterator() {
				LeafQ = new Queue<Node>();
				if(size > 0)
					auxFrontier(root);
				cursor = LeafQ.dequeue();
			}

			private void auxFrontier(Node curr){
				if(curr.left == null && curr.right == null)
					LeafQ.enqueue(curr);
				if(curr.left != null)
					this.auxFrontier(curr.left);
				if(curr.right != null)
					this.auxFrontier(curr.right);
			}
			public int get(){
				if(this.isValid())
					return cursor.key;
				else
					return 0;				
			}

			public void next(){
				if(this.isValid()){
					if(LeafQ.front() != null)
						cursor = LeafQ.dequeue();
					else
						cursor = null;
				}

			}

			public boolean isValid(){
				if(cursor != null)
					return true;
				else
					return false;
			}
		}
	}


