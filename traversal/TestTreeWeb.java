package traversal;
import junit.framework.*;
public class TestTreeWeb extends TestCase {


public void testAdd() {
	Tree foo = new Tree();
	assertEquals("created and empty", 0, foo.size());
	foo.add(5);
	assertEquals("size check: 1", 1, foo.size());
	foo.add(1);
	foo.add(2);
	foo.add(3);
	foo.add(4);
	foo.add(6);
	foo.add(7);
	foo.add(8);
	foo.add(9);
	foo.add(10);
	foo.add(5);//this does not get added since it's duped
	assertEquals("size check: 10", 10, foo.size());
}

public void testBFS() {
	Tree foo = new Tree();
	assertEquals("created and empty", 0, foo.size());
	Iterator it = foo.mkBFSIterator();
	assertEquals("shouldn't be valid", false, it.isValid());
	assertEquals("no key in at cursor", 0, it.get());
	it.next();
	assertEquals("shouldn't be valid", false, it.isValid());
	assertEquals("no key in at cursor", 0, it.get());

	foo.add(5);

	Iterator it2 = foo.mkBFSIterator();
	assertEquals("should be valid", true, it2.isValid());
	assertEquals("key at cursor", 5, it2.get());
	it2.next();
	assertEquals("shouldn't be valid", false, it2.isValid());
	assertEquals("no key in at cursor", 0, it2.get());

	foo.add(1);
	foo.add(2);
	foo.add(3);
	foo.add(4);
	foo.add(6);
	foo.add(7);
	foo.add(8);
	foo.add(9);
	foo.add(10);

	Iterator it3 = foo.mkBFSIterator();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 5, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 1, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 6, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 2, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 7, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 3, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 8, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 4, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 9, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 10, it3.get());
	it3.next();
	assertEquals("shouldn't be valid", false, it3.isValid());
	assertEquals("no key in at cursor", 0, it3.get());

	//make a more balanced tree
	Tree bar = new Tree();
	bar.add(5);
	bar.add(4);
	bar.add(6);
	bar.add(2);
	bar.add(1);
	bar.add(3);
	bar.add(8);
	bar.add(7);
	bar.add(9);
	bar.add(10);

	Iterator it4 = bar.mkBFSIterator();
	assertEquals("should be valid", true, it4.isValid());
	assertEquals("key at cursor", 5, it4.get());
	it4.next();
	assertEquals("should be valid", true, it4.isValid());
	assertEquals("key at cursor", 4, it4.get());
	it4.next();
	assertEquals("should be valid", true, it4.isValid());
	assertEquals("key at cursor", 6, it4.get());
	it4.next();
	assertEquals("should be valid", true, it4.isValid());
	assertEquals("key at cursor", 2, it4.get());
	it4.next();
	assertEquals("should be valid", true, it4.isValid());
	assertEquals("key at cursor", 8, it4.get());
	it4.next();
	assertEquals("should be valid", true, it4.isValid());
	assertEquals("key at cursor", 1, it4.get());
	it4.next();
	assertEquals("should be valid", true, it4.isValid());
	assertEquals("key at cursor", 3, it4.get());
	it4.next();
	assertEquals("should be valid", true, it4.isValid());
	assertEquals("key at cursor", 7, it4.get());
	it4.next();
	assertEquals("should be valid", true, it4.isValid());
	assertEquals("key at cursor", 9, it4.get());
	it4.next();
	assertEquals("should be valid", true, it4.isValid());
	assertEquals("key at cursor", 10, it4.get());
	it4.next();
	assertEquals("shouldn't be valid", false, it4.isValid());
	assertEquals("no key in at cursor", 0, it4.get());
}
public void testDFS() {
	Tree foo = new Tree();
	assertEquals("created and empty", 0, foo.size());
	Iterator it = foo.mkDFSIterator();
	assertEquals("shouldn't be valid", false, it.isValid());
	assertEquals("no key in at cursor", 0, it.get());
	it.next();
	assertEquals("shouldn't be valid", false, it.isValid());
	assertEquals("no key in at cursor", 0, it.get());

	foo.add(5);

	Iterator it2 = foo.mkDFSIterator();
	assertEquals("should be valid", true, it2.isValid());
	assertEquals("key at cursor", 5, it2.get());
	it2.next();
	assertEquals("shouldn't be valid", false, it2.isValid());
	assertEquals("no key in at cursor", 0, it2.get());

	foo.add(4);
	foo.add(6);
	foo.add(2);
	foo.add(1);
	foo.add(3);
	foo.add(8);
	foo.add(7);
	foo.add(9);
	foo.add(10);

	Iterator it3 = foo.mkDFSIterator();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 5, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 4, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 2, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 1, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 3, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 6, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 8, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 7, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 9, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 10, it3.get());
	it3.next();
	assertEquals("shouldn't be valid", false, it3.isValid());
	assertEquals("no key in at cursor", 0, it3.get());
}
public void testPreorder() {
	Tree foo = new Tree();
	assertEquals("created and empty", 0, foo.size());
	Iterator it = foo.mkPreorderIterator();
	assertEquals("shouldn't be valid", false, it.isValid());
	assertEquals("no key in at cursor", 0, it.get());
	it.next();
	assertEquals("shouldn't be valid", false, it.isValid());
	assertEquals("no key in at cursor", 0, it.get());

	foo.add(5);

	Iterator it2 = foo.mkPreorderIterator();
	assertEquals("should be valid", true, it2.isValid());
	assertEquals("key at cursor", 5, it2.get());
	it2.next();
	assertEquals("shouldn't be valid", false, it2.isValid());
	assertEquals("no key in at cursor", 0, it2.get());

	foo.add(4);
	foo.add(6);
	foo.add(2);
	foo.add(1);
	foo.add(3);
	foo.add(8);
	foo.add(7);
	foo.add(9);
	foo.add(10);

	Iterator it3 = foo.mkPreorderIterator();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 5, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 4, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 2, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 1, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 3, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 6, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 8, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 7, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 9, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 10, it3.get());
	it3.next();
	assertEquals("shouldn't be valid", false, it3.isValid());
	assertEquals("no key in at cursor", 0, it3.get());
}
public void testInorder() {
	Tree foo = new Tree();
	assertEquals("created and empty", 0, foo.size());
	Iterator it = foo.mkInorderIterator();
	assertEquals("shouldn't be valid", false, it.isValid());
	assertEquals("no key in at cursor", 0, it.get());
	it.next();
	assertEquals("shouldn't be valid", false, it.isValid());
	assertEquals("no key in at cursor", 0, it.get());

	foo.add(5);

	Iterator it2 = foo.mkInorderIterator();
	assertEquals("should be valid", true, it2.isValid());
	assertEquals("key at cursor", 5, it2.get());
	it2.next();
	assertEquals("shouldn't be valid", false, it2.isValid());
	assertEquals("no key in at cursor", 0, it2.get());

	foo.add(4);
	foo.add(6);
	foo.add(2);
	foo.add(1);
	foo.add(3);
	foo.add(8);
	foo.add(7);
	foo.add(9);
	foo.add(10);

	Iterator it3 = foo.mkInorderIterator();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 1, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 2, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 3, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 4, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 5, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 6, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 7, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 8, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 9, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 10, it3.get());
	it3.next();
	assertEquals("shouldn't be valid", false, it3.isValid());
	assertEquals("no key in at cursor", 0, it3.get());
}
public void testPostorder() {
	Tree foo = new Tree();
	assertEquals("created and empty", 0, foo.size());
	Iterator it = foo.mkPostorderIterator();
	assertEquals("shouldn't be valid", false, it.isValid());
	assertEquals("no key in at cursor", 0, it.get());
	it.next();
	assertEquals("shouldn't be valid", false, it.isValid());
	assertEquals("no key in at cursor", 0, it.get());

	foo.add(5);

	Iterator it2 = foo.mkPostorderIterator();
	assertEquals("should be valid", true, it2.isValid());
	assertEquals("key at cursor", 5, it2.get());
	it2.next();
	assertEquals("shouldn't be valid", false, it2.isValid());
	assertEquals("no key in at cursor", 0, it2.get());

	foo.add(4);
	foo.add(6);
	foo.add(2);
	foo.add(1);
	foo.add(3);
	foo.add(8);
	foo.add(7);
	foo.add(9);
	foo.add(10);

	Iterator it3 = foo.mkPostorderIterator();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 1, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 3, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 2, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 4, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 7, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 10, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 9, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 8, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 6, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 5, it3.get());
	it3.next();
	assertEquals("shouldn't be valid", false, it3.isValid());
	assertEquals("no key in at cursor", 0, it3.get());
}
public void testFrontier() {
	Tree foo = new Tree();
	assertEquals("created and empty", 0, foo.size());
	Iterator it = foo.mkFrontierIterator();
	assertEquals("shouldn't be valid", false, it.isValid());
	assertEquals("no key in at cursor", 0, it.get());
	it.next();
	assertEquals("shouldn't be valid", false, it.isValid());
	assertEquals("no key in at cursor", 0, it.get());

	foo.add(5);

	Iterator it2 = foo.mkFrontierIterator();
	assertEquals("should be valid", true, it2.isValid());
	assertEquals("key at cursor", 5, it2.get());
	it2.next();
	assertEquals("shouldn't be valid", false, it2.isValid());
	assertEquals("no key in at cursor", 0, it2.get());

	foo.add(4);
	foo.add(6);
	foo.add(2);
	foo.add(1);
	foo.add(3);
	foo.add(8);
	foo.add(7);
	foo.add(9);
	foo.add(10);

	Iterator it3 = foo.mkFrontierIterator();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 1, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 3, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 7, it3.get());
	it3.next();
	assertEquals("should be valid", true, it3.isValid());
	assertEquals("key at cursor", 10, it3.get());
	it3.next();
	assertEquals("shouldn't be valid", false, it3.isValid());
	assertEquals("no key in at cursor", 0, it3.get());
}
}