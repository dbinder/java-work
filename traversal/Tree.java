package traversal;

public class Tree {
	
	private class Node
	{
		private int key;
		private Node left;
		private Node right;
		
		public Node(int key)
		{
			this.key = key;
			this.left = null;
			this.right = null;
			
		}
		
		public Node(int key, Node left, Node right)
		{
			this.key=key;
			this.left = left;
			this.right = right;
		}
	}
	
	private Node root;
	private int size;
	
	public Tree()
	{
		root = null;
		size = 0;
	}
	
	
	
	public int size()
	{
		return size;
	}
	
	public void add(int i)
	{
		addAux(i, null, root);
	}
	
	private void addAux(int i, Node parent, Node node)
	{
		
		if(parent == null && node == null)//empty tree
		{
			root = new Node(i);
			size++;
			return;
		}
		
		if(node == null)
		{
			Node tmp = new Node(i);
			if(i < parent.key)
				parent.left = tmp;
			else
				parent.right = tmp;
			size++;
			return;

		}	
		if(i == node.key)//Identical check
			return;
		if(i < node.key)
			addAux(i, node, node.left);
		else
			addAux(i, node, node.right);
		
	}

	public Iterator mkBFSIterator()
	{
		return new BFSIterator();
	}

	public Iterator mkDFSIterator()
	{
		return new DFSIterator();
	}

	public Iterator mkPreorderIterator()
	{
		return new PreorderIterator();
	}

	public Iterator mkInorderIterator()
	{
		return new InorderIterator();
	}

	public Iterator mkPostorderIterator()
	{
		return new PostorderIterator();
	}

	public Iterator mkFrontierIterator()//Iterators over the leaves left to right
	{
		return new FrontierIterator();
	}

	public class BFSIterator implements Iterator
	{	
		private Node cursor;
		private Queue<Node> BFSQueue;
		
		public BFSIterator()
		{
			cursor = root;
			BFSQueue = new Queue<Node>();
		}
		
		public int get()
		{	if (cursor != null)
				return cursor.key;
			else
				return 0;
		}
		
		public boolean isValid()
		{	if(cursor != null)
				return true;
			else 
				return false;
		}	
		
		public void next()
		{
			
		}
	
	}

	public class DFSIterator implements Iterator
	{

		@Override
		public int get() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public void next() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean isValid() {
			// TODO Auto-generated method stub
			return false;
		}
		
	}
	
	public class PreorderIterator implements Iterator
	{

		@Override
		public int get() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public void next() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean isValid() {
			// TODO Auto-generated method stub
			return false;
		}
		
	}
	
	public class InorderIterator implements Iterator
	{

		@Override
		public int get() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public void next() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean isValid() {
			// TODO Auto-generated method stub
			return false;
		}
		
	}
	
	public class PostorderIterator implements Iterator
	{

		@Override
		public int get() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public void next() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean isValid() {
			// TODO Auto-generated method stub
			return false;
		}
		
	}
	
	public class FrontierIterator implements Iterator
	{

		@Override
		public int get() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public void next() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean isValid() {
			// TODO Auto-generated method stub
			return false;
		}
		
	}
	
}


