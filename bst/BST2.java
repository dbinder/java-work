package bst;

public class BST2 {
	public class BST<K extends Comparable<K>, V extends Comparable<V>> {
		private class Node {
			public K key;
			public V value;
			public Node left;
			public Node right;

			public Node(K key, V value, Node left, Node right){
				this.key = key;
				this.value = value;
				this.left = left;
				this.right = right;
			}

			public Node(K key, V value){
				this.key = key;
				this.value = value;
				this.left = null;
				this.right = null;
			}
		}

		private Node root;
		private int size;
		private K valueSearchKey = null;//ugly hack to do revFind

		public BST(){
			root = null;
			size = 0;
		}

		public int size() {
			return this.size;
		}

		public void add(K key, V value){
//			void add(K key, V value) Insert an element into the tree. There will be only one
//			place in the tree where it should be inserted. This should run in O(log n) time.
//			If the key already exists in the tree, replace the value with the new one.

			addAux(key, value, null, root);

		}
		private void addAux(K key, V value, Node parent, Node node){
			if(key!=null){
				if(parent == null && node == null){//empty tree
					this.root = new Node(key, value);
					size++;
					return;
				}
				if(node == null){//empty node, yay
					Node tmp = new Node(key, value);
					if(key.compareTo(parent.key)<0)
						parent.left = tmp;
					else
						parent.right = tmp;
					size++;
					return;
				}
				//identical key update
				if(key.compareTo(node.key)==0){
					node.value = value;
					return;
				}
				//keep going further
				if(key.compareTo(node.key)<0)
					addAux(key, value, node, node.left);
				else
					addAux(key, value, node, node.right);
			}
		}
		public V find(K key){
//			V find(K key) Try to find key K, and return value V if it exists, null otherwise.
//			This should run in O(log n) time.
			return findAux(key,root);
		}
		private V findAux(K key, Node node){
			if(node==null)
				return null;
			if(node.key.compareTo(key)==0)
				return node.value;
			if(key.compareTo(node.key)<0)
				return findAux(key,node.left);
			else
				return findAux(key,node.right);
		}
		public void delete(K key){
//			void delete(K key) Delete element k, if it exists. This should run in O(log n) time.
//			There are at least four kinds of cases you need to test.

			/* 
			 * 1. return if size=0
			 * 2. otherwise loop thru until we find a key
			 * 3. or fail and exit
			 * 4. size-- if removed successfully;
			 */
			if(size < 1)
				return;
			else if(size == 1){
				if(key.compareTo(root.key)==0){
					root = null;
					size--;
				}
			}else
				deleteAux(key, null, root);
		}
		private void deleteAux(K key, Node parent, Node node){
			if(key.compareTo(node.key)==0){//TONIGHT, WE DINE IN HELL
				if(parent == null){//deleting root
					if(node.left == null){//right node only?
						//**node = node.right;
						root.key = node.right.key;
						root.value = node.right.value;
						root.left = node.right.left;
						root.right = node.right.right;
						size--;
					}
					else if(node.right == null){//left node only?
						//**node = node.left;
						root.key = node.left.key;
						root.value = node.left.value;
						root.left = node.left.left;
						root.right = node.left.right;
						size--;
					}
					else{//both nodes present, use gr. pred.
						Node gp = root.left;
						//keep moving right
						while(gp.right != null)
							gp = gp.right;
						//now that we stopped, copy this leaf/single-child node and delete original
						Node tmp = new Node(gp.key, gp.value, root.left, root.right);
						this.delete(gp.key);
						root = tmp;
					}
				}
				else{//deleting a node elsewhere
					if(node.left==null){//no left node, can we go right?
						if(node.right == null){//no children, sweet!
							if(parent.left == node)
								parent.left = null;
							else
								parent.right = null;
							size--;
						}else{
							//right child
							//**node = node.right;
							node.key = node.right.key;
							node.value = node.right.value;
							node.left = node.right.left;
							node.right = node.right.right;
							size--;
						}
					}else if(node.right == null){
						//left child
						//**node = node.left;
						node.key = node.left.key;
						node.value = node.left.value;
						node.right = node.left.right;
						node.left = node.left.left;
						size--;
					}else{
						//both node children present, use greatest predecessor
						//move left once
						Node gp = node.left;
						//keep moving right
						while(gp.right != null)
							gp = gp.right;
						//now that we stopped, copy this leaf/single-child node and delete original
						Node tmp = new Node(gp.key, gp.value);
						this.delete(gp.key);
						node.key = tmp.key;
						node.value = tmp.value;
					}
				}
			}
			//key not present
			else if(key.compareTo(node.key)<0)
				deleteAux(key, node, node.left);
			else
				deleteAux(key, node, node.right);
		}

		public K revFind(V value){
//			K revFind(V value) This is a "reverse phone number lookup" method. You sup-
//			ply the value, and get the key back. The values are not in any order, so you'll
//			have to use one of the traversal patterns to find it. For this lab, use depth-first
//			search.

			/* 
			 * To traverse with DFS:
			 *    1. Visit the node.   
			 *    2. Traverse the left subtree.   
			 *    3. Traverse the right subtree.
			 *    
			 *   (From WIKIPEDIA)
			 * 1. visit node and compare values's return key
			 * 2. recurse left
			 * 3. recurse right
			 * 4. if nothing returned, return null
			 */
			valueSearchKey = null;
			if(root != null)
				preorder(value,root);
			return valueSearchKey;
		}
		private void preorder(V value, Node node){
			if(value.compareTo(node.value)==0){
				valueSearchKey = node.key;
				return;
			}
			if(node.left != null)
				this.preorder(value, node.left);
			if(node.right != null)
				this.preorder(value, node.right);
		}
		public Iterator<K> mkBFSIterator() {
			return new BFSIterator();
		}

		public class BFSIterator implements Iterator<K>{
//			Iterator<K> mkBFSIterator() One of the things you want to do is make sure
//			that the elements are placed correctly in the tree. If you make a breadth-first
//			search iterator, you can iterate through the elements of the tree and see if they
//			are in the order you expect. You may want to copy your Queue.java over here
//			and make use of that. Be sure to add it to the repository if you do.

			Node cursor;
			Queue<Node> BFSQueue;
			public BFSIterator() {
				cursor = root;
				BFSQueue = new Queue<Node>();
			}

			public K get(){
				if(cursor !=null)
					return cursor.key;
				else
					return null;
			}

			public void next(){
				/*
				 * 1. enqueue children
				 * 2. dequeue next
				 * 3. set yourself to what you dequeued if not null, otherwise null
				 */
				if(this.isValid()){
					if(cursor.left != null)
						BFSQueue.enqueue(cursor.left);
					if(cursor.right != null)
						BFSQueue.enqueue(cursor.right);

					if(BFSQueue.front() == null)
						cursor = null;
					else
						cursor = BFSQueue.dequeue();
				}
			}

			public boolean isValid() {
				if(cursor != null)
					return true;
				else
					return false;
			}

			public void delete(){
				if(this.isValid())
					BST.this.delete(cursor.key);
			}
		}
	}

}
