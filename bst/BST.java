package bst;
public class BST<K extends Comparable, V extends Comparable> {

	//inner node class
	private class Node {
		public K key;
		public V value;
		public Node left;
		public Node right;

		public Node(K key, V value) {
			this.key = key;
			this.value = value;
		}

		public Node(K key, V value, Node left, Node right) {
			this.key = key;
			this.value = value;
			this.left = left;
			this.right = right;
		}
	}
	
    //vars
	private int size;
	private Node root;
	private K searchKey;
	//constructor
	public BST() {
		size = 0;
		root = null;
	}
	
	//returns size
    public int size() {
    	return size;
    }
    
    //adds a new node
    public void add(K key, V value) 
    {
    	 addAux(key, value, null, root);
    }
    private void addAux(K key, V value, Node parent, Node node)
    {
    	if(key != null)
    	{
    		if(parent == null && node == null)//empty tree
    		{   
    			this.root = new Node(key, value);
    			size++;
    			return;
    		}
    		
    		if(node == null)
    		{
    			Node temp = new Node(key, value);
    			if(key.compareTo(parent.key) < 0)
    				parent.left = temp;
    			else
    				parent.right = temp;
    			size++;
    			return;
    		}
    		
    		if(key.compareTo(node.key) == 0)
    		{	
    			node.value = value;
    			return;
    		}
    		
    		if(key.compareTo(node.key) < 0)
    			addAux(key, value, node, node.left);
    		else
    			addAux(key, value, node, node.right);
    		
    	}
    		
    }
    //finds a node
    public V find(K key)
    {
    	return findAux(key, root);
    }
    	
    private V findAux(K key, Node node)
    {	    	
    		if (node == null)
    			return null;
    		if(node.key.compareTo(key) == 0)
    			return node.value;
    		if(key.compareTo(node.key) < 0)
    			return findAux(key, node.left);
    		else
    			return findAux(key, node.right);
    }
    
    //deletes a node
    public void delete(K key)
    {
    	
    	if (size < 1)
    		return;
    	else if(size == 1)
    	{
    		if (key.compareTo(root.key) == 0 )
    		{
    			root = null;
    			size--;
    			
    		}
    	}else
    		deleteAux(key, null, root);
    			
    		    
    		
    	
    		
    	
    }

    private void deleteAux(K key, Node parent, Node node)
    {
    	if(key.compareTo(node.key) == 0)
    	{
    		
    		if(parent == null )//Deleting root;
    		{
    			if(node.left == null)
    			{
    				//only a right node left
    				root.key = node.right.key;
    				root.value = node.right.value;
    				root.left = node.right.left;
    				root.right = node.right.right;
    				size--;
    				mkBFSIterator();
    			}
    			else if(node.right == null)
    			{
    				//only a left node remaining
    				root.key = node.left.key;
    				root.value = node.left.value;
    				root.left = node.left.left;
    				root.right = node.left.right;
    				size--;
    				mkBFSIterator();
    			}
    			else//both nodes present
    			{
    				Node grp = root.left;
    				while(grp.right != null)//bottom of tree going left one step first then down the right path;
    					grp = grp.right;
    				Node temp = new Node(grp.key, grp.value, root.left, root.right);
    				delete(grp.key);
    				root = temp;
    				mkBFSIterator();
    				
    			}
    			
    		}	
    		else
    		{
    			if(node.left == null)
    			{
    				if(node.right == null)
    				{
    					if(parent.left == node)
    						parent.left = null;
    					else
    						parent.right = null;
    					size--;
    					
    				}else
    				{
    					node.key = node.right.key;
    					node.value = node.right.value;
    					node.left = node.right.left;
    					node.right = node.right.right;
    					size--;
    					this.mkBFSIterator();
    				}
    			}else if(node.right == null)
				{
					node.key = node.left.key;
					node.value = node.left.value;
					node.right = node.left.right;
					node.left = node.left.left;
					size--;
					
				}else
				{
					Node grp = node.left;
					
					while(grp.right != null)
						grp = grp.right;
					Node temp = new Node(grp.key, grp.value);
					this.delete(grp.key);
					node.key = temp.key;
					node.value = temp.value;
					
					
				}
    		
    		}
    		
    	}
    	else if (key.compareTo(node.key)< 0 )
    		{
    			deleteAux(key, node, node.left);
    			
    		}
    	else
    		{
    			deleteAux(key, node, node.right);
    			
    		}
    	
    	
    }
    //reverse find for key, uses the depth-first search
    public K revFind(V value) {

		searchKey = null;
		if(root != null)
			preorder(value,root);
		return searchKey;
	}
	private void preorder(V value, Node node){
		if(value.compareTo(node.value)==0){
			searchKey = node.key;
			return;
		}
		if(node.left != null)
			this.preorder(value, node.left);
		if(node.right != null)
			this.preorder(value, node.right);
	}
    
    
    //breadth-first iterator
    public Iterator<K> mkBFSIterator() {
    	return new BFSIterator();
    }
    public class BFSIterator implements Iterator<K>{
//		Iterator<K> mkBFSIterator() One of the things you want to do is make sure
//		that the elements are placed correctly in the tree. If you make a breadth-first
//		search iterator, you can iterate through the elements of the tree and see if they
//		are in the order you expect. You may want to copy your Queue.java over here
//		and make use of that. Be sure to add it to the repository if you do.

		Node cursor;
		Queue<Node> BFSQueue;
		public BFSIterator() {
			cursor = root;
			BFSQueue = new Queue<Node>();
		}

		public K get(){
			if(cursor !=null)
				return cursor.key;
			else
				return null;
		}

		public void next(){
			
			if(this.isValid()){
				if(cursor.left != null)
					BFSQueue.enqueue(cursor.left);
				if(cursor.right != null)
					BFSQueue.enqueue(cursor.right);

				if(BFSQueue.front() == null)
					cursor = null;
				else
					cursor = BFSQueue.dequeue();
			}
		}

		public boolean isValid() {
			if(cursor != null)
				return true;
			else
				return false;
		}

		public void delete(){
			if(this.isValid())
				BST.this.delete(cursor.key);
		}
	}

}


