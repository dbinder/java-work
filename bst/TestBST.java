package bst;
import junit.framework.*;

public class TestBST extends TestCase {
    public String n1  = "Jenny";
    public String n2  = "Empire";
    public String n3  = "Emergency";
    public String n4  = "Helpdesk";
    public String n5  = "Information";
    public String n6  = "Call of Duty 4";
    public String n7  = "Medal of Honor";
    public String n8  = "Valkyria Chronicles";
    public String n9  = "Assasins Creed";
    public String n10 = "Red Dead Redemption";
    public String n11 = "Soul Calibur IV";
    public String n12 = "Grand Theft Auto IV";
    public String n13 = "Resonance of Fate";
    public String n14 =	"Mass Effect";
    public String n15 = "Overlord II";
    
    public Integer i1  = new Integer(8675309);
    public Integer i2  = new Integer(5882300);
    public Integer i3  = new Integer(911);
    public Integer i4  = new Integer(5673375);
    public Integer i5  = new Integer(5551212);
    public Integer i6  = new Integer(30072);
    public Integer i7  = new Integer(30436125);
    public Integer i8  = new Integer(30196);
    public Integer i9  = new Integer(3008922);
    public Integer i10 = new Integer(1831139813);
    public Integer i11 = new Integer(45440750);
    public Integer i12 = new Integer(1831139012);
    public Integer i13 = new Integer(68039);
    public Integer i14 = new Integer(135900033);
    public Integer i15 = new Integer(49402656);
    
    public TestBST(String name) {
        super(name);
    }

    public void testCreate() {
        BST<String,Integer> bst = new BST<String,Integer>();

        assertEquals("1. create",0,bst.size());
      
        	
    }
    
    public void testEmpty()
    {
    	BST <String, Integer> foo = new BST<String,Integer>();
    	Iterator<String> first = foo.mkBFSIterator();
    	assertEquals("Empty Test 0.1",0,foo.size());
    	foo.delete(n1);
    	assertEquals("Empty Test 0.2",0,foo.size());
    	assertEquals("Empty Test 0.3",null,foo.find(n1));
    	assertEquals("Empty Test 0.4",null,first.get());
    	assertEquals("Empty Test 0.5",false,first.isValid());
    	first.delete();
    	assertEquals("Empty Test 0.6",false,first.isValid());
    	assertEquals("Empty Test 0.7",null, first.get());
    	
    }
    
    public void testOneEle()
    {
    	BST <String, Integer> foo = new BST<String,Integer>();
    	assertEquals("One Element Test 0.1",0,foo.size());
    	foo.add(n1, i1);
    	Iterator<String> first = foo.mkBFSIterator();
    	assertEquals("One Element Test 0.2",1,foo.size());
    	assertEquals("One Element Test 0.3",i1,foo.find(n1));
    	assertEquals("One Element Test 0.4",n1,foo.revFind(i1));
    	assertEquals("One Element Test 0.5",n1,first.get());
    	assertEquals("One Element Test 0.6",true,first.isValid());
    	first.next();
    	assertEquals("One Element Test 0.7",false,first.isValid());
    	
    	foo.delete(n1);
    	
    	assertEquals("One Element Test 0.8",0,foo.size());
    	assertEquals("One Element Test 0.9",null,first.get());
    	assertEquals("One Element Test 1.0",null,foo.find(n1));
    	
    }
   

    public void testMultipleEle()
    {
            BST <String, Integer> foo = new BST<String, Integer>();
            foo.add(n4,i4);
            foo.add(n1,i1);
            foo.add(n2,i2);
            foo.add(n3,i3);
            foo.add(n5,i5);
            Iterator<String> first = foo.mkBFSIterator();

            assertEquals("Mult Element Test 0.1",5,foo.size());
            assertEquals("Mult Element Test 0.2",n4,first.get());
            assertEquals("Mult Element Test 0.3",i5,foo.find(n5));
            assertEquals("Mult Element Test 0.4",n5,foo.revFind(i5));
            assertEquals("Mult Element Test 0.5",true,first.isValid());

            foo.delete(n4);

            assertEquals("Mult Element Test 0.6",null,foo.find(n4));
            assertEquals("Mult Element Test 0.7",null,foo.revFind(i4));
            assertEquals("mult Element Test 0.8",4,foo.size());
          ///  assertEquals("Mult Element Test 0.9",)
            assertEquals("Mult Element Test 0.9",n2,first.get());
            first.next();
            assertEquals("Mult ELement Test 1.0",n3,first.get());
            first.next();
            assertEquals("Mult Element Test 1.1",n1,first.get());
            first.next();
            assertEquals("Mult Element Test 1.2",n5,first.get());
            first.next();
            assertEquals("Mult Element Test 1.3",null,first.get());
            first.next();
            assertEquals("Mult Element Test 1.4",null,first.get());
    }
    
    public void testStraightTree()
    {	
    	  BST <String, Integer> foo = new BST<String, Integer>();
    	
    	  foo.add(n9,i9);
    	  foo.add(n6, i6);
    	  foo.add(n3,i3);
    	  foo.add(n2, i2);
    	  foo.add(n12, i12);
    	  foo.add(n4, i4);
    	  foo.add(n5, i5);
    	  foo.add(n1, i1);
    	  foo.add(n14, i14);
    	  foo.add(n7, i7);
    	  foo.add(n15, i15);
    	  foo.add(n10, i10);
    	  foo.add(n13, i13);
    	  foo.add(n11, i11);
    	  foo.add(n8,i8);
    	  //n9,n6,n3,n2,n12,n4,n5,n1,n14,n7,n15,n10,n13,n11,n8
    	  //order for a Straight tree
    	 
    
    }
}





