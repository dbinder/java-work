package avl;

interface Iterator<E>
{
  E Get();
  void Next();
  boolean IsValid();
  void Delete();
}
