package avl;

public class Queue<E>
{
  private Node first = null;
  private Node last = null;
  private E front = null;
  int size = 0;

  public void Queue()
  {
    first = null;
    last = first;
    front = first.data;
    size = 0;
  }
  private class Node
  {
  public E data;
    public Node next;
    public Node(E data)
    {
      this.data = data;
    }

    public Node(E data, Node next)
    {
      this.data = data;
      this.next = next;
    }
 	}

  public E front()
  {
  if(first == null)
      return null;
    else
    front = first.data;
    return front;
  }

  public int size()
  {
    return size;
  }
  public void enqueue(E data)
  {
    Node temp = new Node(data);
    size++;
    if (last == null)
    {
      first = temp;
      last = temp;
    }
    else
    {
      last.next = temp;
      last = temp;
    }
    front = first.data;
  }

  public E dequeue()
  {
    E temp;
  if(first == null)
      return null;
    else
    {
      temp = first.data;
      first = first.next;
      size--;
    if (first == null)
        last = null;
      return temp;
    }
  }
}
