package avl;

public class TestAVL<K extends Comparable, V extends Comparable>
{
  private class Node 
  {
    public K key;
    public V value;
    public Node left;
    public Node right;

    public Node(K key, V value) 
    {
      this.key = key;
      this.value = value;
    }

    public Node(K key, V value, Node left, Node right)
    {
      this.key = key;
      this.value = value;
      this.left = left;
      this.right = right;
    }
  }
}
