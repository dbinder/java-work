package avl;

public class AVL <K extends Comparable, V extends Comparable>
{

  private class Node 
  {
    public K key;
    public V value;
    public Node left;
    public Node right;

    public Node(K key, V value) 
    {
      this.key = key;
      this.value = value;
    }

    public Node(K key, V value, Node left, Node right) 
    {
      this.key = key;
      this.value = value;
      this.left = left;
      this.right = right;
    }
  }

  public Iterator mkBFSIterator()
  {
    return new BFSIterator();
  }

  public Iterator mkDFSIterator()
  {
    return new DFSIterator();
  }

  public class BFSIterator implements Iterator<Object>
  {
    private Node m_cursor;
    private Node m_root;
    private Queue<Node> BFSQueue;
    public BFSIterator()
    {
      m_cursor = m_root;
      BFSQueue = new Queue<Node>();
    }

    @Override public K Get()
    {
      if(m_cursor != null)
      {
        return m_cursor.key;
      }
      else
      {
        return null;
      }
    }

    @Override public void Next()
    {
      if(this.IsValid())
      {
        if(m_cursor.left != null)
          BFSQueue.enqueue(m_cursor.left);
        if(m_cursor.right != null)
          BFSQueue.enqueue(m_cursor.right);
        if(BFSQueue.front() == null)
          m_cursor = null;
        else
          m_cursor = BFSQueue.dequeue();
      }
    }

    @Override public boolean IsValid()
    {
      if(m_cursor != null)
        return true;
      else
        return false;
    }
    
    @Override public void Delete(){}
  }


  public class DFSIterator implements Iterator
  {
    private Node cursor;
    private Queue<Node> DFSQ;
    
    public DFSIterator()
    {
      DFSQ = new Queue<Node>();
        //if(size > 0)
         // auxDFS(root);
        cursor = DFSQ.dequeue();
    }

    private void auxDFS(Node curr)
    {
      DFSQ.enqueue(curr);
      if(curr.left != null)
        this.auxDFS(curr.left);
      if(curr.right != null)
        this.auxDFS(curr.right);
    }

    public K Get()
    {
      if(this.IsValid())
        return cursor.key;
      else
        return null;
    }

    public void Next()
    {
      if(this.IsValid())
      {
        if(DFSQ.front() != null)
          cursor = DFSQ.dequeue();
        else
          cursor = null;
      }
    }

    public boolean IsValid()
    {
      if(cursor != null)
        return true;
      else
        return false;
    }
    
    @Override public void Delete(){}
  }
}

