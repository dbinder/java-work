package doublylinkedlists;


import junit.framework.*;

public class TestDList extends TestCase {
    public Integer i1 = new Integer(1);
    public Integer i2 = new Integer(2);
    public Integer i5 = new Integer(5);
    public Integer i10 = new Integer(10);
    public Integer i15 = new Integer(15);
    public Integer i20 = new Integer(20);
    public Integer i25 = new Integer(25);

    public TestDList(String name) {
        super(name);
    }

    public void testCreate()
         {
                DList<Integer> foo = new DList<Integer>();

                assertEquals("1. create",0,foo.size());

                foo.insertFront(i25);
                assertEquals("Test add in front 0.1",1,foo.size());

                foo.insertFront(i1);
                assertEquals("Test add in front 0.2",2,foo.size());

                foo.insertFront(i1);
                assertEquals("Test add in front 0.3",3,foo.size());

                foo.insertFront(i1);
                assertEquals("Test add in front 0.4",4,foo.size());

         }

        public void testEmpty()
        {
                DList<Integer> foo = new DList<Integer>();

                assertEquals("Empty test 0.1",0,foo.size());
                
                Iterator<Integer> fwd = foo.makeFwdIterator();
                Iterator<Integer> rev = foo.makeRevIterator();
                
                assertEquals("FW cursor isValid test 0.1",false,fwd.isValid());
                assertEquals("RV cursor isValid test 0.1",false,rev.isValid());
                assertEquals("FW cursor get test 0.1",null,fwd.get());
                assertEquals("RV cursor get test 0.1",null,rev.get());
                
                fwd.next();
                rev.next();
               
                assertEquals("FW cursor isValid test 0.2",false,fwd.isValid());
                assertEquals("RV cursor isValid test 0.2",false,rev.isValid());
                assertEquals("FW cursor get test 0.2",null,fwd.get());
                assertEquals("RV cursor get test 0.2",null,rev.get());
                
                fwd.delete();
                rev.delete();
                
                assertEquals("FW cursor isValid test 0.3",false,fwd.isValid());
                assertEquals("RV cursor isValid test 0.3",false,rev.isValid());
                assertEquals("FW cursor get test 0.3",null,fwd.get());
                assertEquals("RV cursor get test 0.3",null,rev.get());
                
                fwd.next();
                rev.next();
                
                assertEquals("FW cursor isValid test 0.4",false,fwd.isValid());
                assertEquals("RV cursor isValid test 0.4",false,rev.isValid());
                assertEquals("FW cursor get test 0.4",null,fwd.get());
                assertEquals("RV cursor get test 0.4",null,rev.get());
                
                foo.deleteEnd();
                
                assertEquals("deleteEnd Test 0.1",0,foo.size());
                
                foo.deleteFront();
                
                assertEquals("deleteFront Test 0.1",0,foo.size());
        }

        public void testOneElem()
        {
        		DList<Integer> foo = new DList<Integer>();
        		
        		assertEquals("Empty test 0.2",0,foo.size());
        		Iterator<Integer> fwd = foo.makeFwdIterator();
        		Iterator<Integer> rev = foo.makeRevIterator();
        		foo.insertFront(i1);
        		
        		assertEquals("insertFront Test 0.1",1,foo.size());
        		
        		foo.deleteEnd();
        		
        		assertEquals("deleteEnd Test 0.2",0,foo.size());
        		
        		foo.insertFront(i1);
        		
        		assertEquals("insertFront Test 0.2",1,foo.size());
        		
        		foo.deleteFront();
        		
        		assertEquals("deleteFront Test 0.2",0,foo.size());
        		
        		  foo.deleteFront();

                  assertEquals("deleteFront test 0.2",0,foo.size());

                  assertEquals("Empty test 0.3",0,foo.size());

                  foo.insertEnd(i2);

                  assertEquals("insertEnd test 0.1",1,foo.size());

                  assertEquals("FW cursor isValid test 0.5",false,fwd.isValid());
                  assertEquals("RV cursor isValid test 0.5",false,rev.isValid());
                  assertEquals("FW cursor get test 0.5",null,fwd.get());
                  assertEquals("RV cursor get test 0.5",null,fwd.get());

                  fwd.next();
                  rev.next();



                  assertEquals("FW cursor isValid test 0.6",false,fwd.isValid());
                  assertEquals("RV cursor isValid test 0.6",false,rev.isValid());
                  assertEquals("FW cursor get test 0.6",null,fwd.get());
                  assertEquals("RV cursor get test 0.6",null,rev.get());

                  //new  iterator

                  Iterator<Integer> fwd1 = foo.makeFwdIterator();
                  Iterator<Integer> rev1 = foo.makeRevIterator();

                  assertEquals("FW cursor isValid test 0.7",true,fwd1.isValid());
                  assertEquals("RV cursor isValid test 0.7",true,rev1.isValid());
                  assertEquals("FW cursor get test 0.7",i2,fwd1.get());
                  assertEquals("RV cursor get test 0.7",i2,rev1.get());

                  fwd1.next();
                  rev1.next();
                  //assertEquals("Test",false,fwd.isValid());
                  assertEquals("FW cursor isValid test 0.8",false,fwd1.isValid());
                  assertEquals("RV cursor isValid test 0.8",false,rev1.isValid());
                  assertEquals("FW curosr get test 0.8",null,fwd1.get());
                  assertEquals("RV cursor get test 0.8",null,rev1.get());

                  assertEquals("insertEnd test 0.2",1,foo.size());

                  //new iterators for deletion of last element
                  //fwd first
                  Iterator<Integer> fwd2 = foo.makeFwdIterator();



        }
        
        public void testFind()
        {
        		DList<Integer> foo = new DList<Integer>();
        	
        	 Iterator<Integer> revf = foo.makeRevFindIterator(i1);
             Iterator<Integer> fwdf = foo.makeFwdFindIterator(i1);
             
             assertEquals("RvF is null 0.1",null,revf.get());
             assertEquals("FwdF is null 0.1",null,fwdf.get());
             
        }
        
}
