package doublylinkedlists;

//plus 5 lines 

public class DList<E extends Comparable> {

    private class Node
    {
        public E data;
        public Node next;
        public Node prev;

        public Node(E data) {
            this.data = data;
        }

        public Node(Node prev,E data,Node next) {
                this.prev = prev;
                this.data = data;
                this.next = next;

        }

    }

    private int size;
    private Node first;
    private Node last;
    private Node sentinel;

    public DList() {
        size = 0;
        sentinel = new Node(null);
        sentinel.next = sentinel;
        sentinel.prev = sentinel;
        first = null;
        last = null;
    }

    public int size() 
    {
        return size;
    }
        public void insertFront(E data)
        {
 
        	 sentinel.next = new Node(sentinel, data, sentinel.next);
                sentinel.next.next.prev = sentinel.next;

                first = sentinel.next;
                if (size == 0)
                {
                        last = sentinel.next;
                        sentinel.prev = sentinel.next;

                }
                //else
                //{//
                       // last = sentinel.prev;
              //  }
                size++;
        }

    private abstract class AllIterator implements Iterator<E> {
        protected Node cursor;
        protected int index = 0;
        public E get() {
            if (cursor != null)
                return cursor.data;
            else
                return null;
        }

        public boolean isValid() {
            return (cursor != null);
        }

        public abstract void next();

        public void delete()
        {
                if(get()==null)
                        return;
                cursor.prev.next = cursor.next;
                cursor.next.prev = cursor.prev;
                size--;
                if(!isValid())
                {
                        cursor = null;
                        return;
                }
                next();
                    // your code here
            // What are your boundary conditions?
        }
    }
    
    //have to make iterator after insertions or make the list
    
    public Iterator<E> makeFwdIterator()
    {
        return new FwdIterator();
    }

    public Iterator<E> makeRevIterator()
    {
        return new RevIterator();
    }

    public Iterator<E> makeFwdFindIterator(E data)
    {
        return new FwdFindIterator(data);
    }

    public Iterator<E> makeRevFindIterator(E data)
    {
        return new RevFindIterator(data);
    }

    public class FwdIterator extends AllIterator {
        public FwdIterator() {
            cursor = first;
        }

        public void next() {
            if (cursor != null)
                cursor = cursor.next;
        }
    }
        public class RevIterator extends AllIterator
        {
                public RevIterator()
                {
                        cursor = last;
                }

                public void next()
                {
                        if (cursor != null)
                        cursor = cursor.prev;
                }
        }

        public class FwdFindIterator extends FwdIterator
        {
                E find;
                public FwdFindIterator(E data)
                {
                        cursor = first;
                        find = data;
                        boolean found = false;

                        while(!found && index < size)
                        {
                                if(cursor.data.compareTo(find) == 0)
                                {
                                        found = true;
                                }
                                else
                                {
                                        cursor = cursor.next;
                                        index++;
                                }

                        }
                        if(!found)
                        cursor = null;
                }
        }

        public class RevFindIterator extends RevIterator
        {
                E find;
                public RevFindIterator(E data)
                {
                	
                        cursor = last;
                        find = data;
                        boolean found = false;

                        while(!found && index < size)
                        {
                                if(cursor.data.compareTo(find) == 0)
                                {
                                        found = true;
                                }
                                else
                                {
                                index++;
                                cursor = cursor.prev;

                                }}
                        
                                if(!found)
                                {
                                	cursor = null;
                                }
                        }

                }
        

        /*blic void insertFront(E data)
        {
                Node inNode = new Node(sentinel, data, sentinel.next);
                //sentinel.next = new Node(sentinel, data, sentinel.next);
                sentinel.next.prev = inNode;
                first = inNode;
                if(size == 0)
                {
                        last = inNode;
                        sentinel.prev = inNode;
                }
                else
                {
                        last = sentinel.prev;
                }

                size++;

        }*/

        public void insertEnd(E data)
        {		
        		Node inNode = new Node(sentinel.prev, data, sentinel);
        		sentinel.prev.next = inNode;
        		sentinel.prev = inNode;
        		
               // sentinel.prev = new Node(sentinel.prev, data, sentinel);
              //  sentinel.prev.prev.next = sentinel.prev;
                last = inNode;
                if(size == 0)
                {
                        first = inNode;
                        sentinel.next = inNode;
                }
                size++;
        }

        void deleteFront()
        {
                if(size < 1)
                return;
                sentinel.next.next.prev = sentinel;
                sentinel.next = sentinel.next.next;

                if (size > 1)
                        first = first.next;
                if(size == 1)
                {
                        first = null;
                        last = null;
                }
                size--;
        }

        void deleteEnd()
        {
                if (size < 1)
                return;
                sentinel.prev.prev.next = sentinel;
                sentinel.prev = sentinel.prev.prev;
                if (size > 1)
                {
                        last = last.prev;
                        sentinel.prev.prev = sentinel.prev;
                }

                if (size == 1)
                {
                        first = null;
                        last = null;
                }
                size--;

        }
}
