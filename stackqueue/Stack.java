package stackqueue;

public class Stack<E> extends List{
   private Node top = null;
   private int size;

//make empty stack
        public Stack()
        {
        top = new Node(null);
        size = 0;
        }

//not sure
        private class Node
        {
                public E data;
                public Node next;

                public Node(E data)
                {
                        this.data = data;
                }

                public Node(E data, Node next)
                {
                        this.data = data;
                        this.next = next;
                }
        }

//top
        public E  top()
        {
                return  top.data;
        }

//push on to the Stack
        void push(E elt)
        {
                top = new Node(elt, top);
                size++;
        }

//pop from stack
        public E pop()
        {
                E t = top.data;
                top = top.next;
                size--;
                return t;
        }

//size of stack
        public int size()
        {
                if (size < 0)
                {
                        size = 0;
                        return size;
                }else
                return size;
        }





}
