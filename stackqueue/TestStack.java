package stackqueue;

import junit.framework.*;

public class TestStack extends TestCase {
    public Integer i1 = new Integer(1);
    public Integer i2 = new Integer(2);
    public Integer i5 = new Integer(5);
    public Integer i10 = new Integer(10);
    public Integer i15 = new Integer(15);
    public Integer i20 = new Integer(20);
    public Integer i25 = new Integer(25);

    public TestStack(String name) {
        super(name);
    }


        //empty test
        public void testEmpty()
        {
        //Create and empty Stack
        Stack<Integer> foo = new Stack<Integer>();
        //is it empty
        assertEquals("Empty Test 1",null,foo.top());
        assertEquals("Empty Test 2",0,foo.size());

        }


        public void testPop()
        {
        Stack<Integer> foo = new Stack<Integer>();
        assertEquals("Empty  Test 1",null,foo.top());
        assertEquals("Empty  Size Test 1",0,foo.size());

        foo.push(i1);
        assertEquals("Push Size Test 1",1,foo.size());
        assertEquals("Push Test 1",i1,foo.top());
        foo.pop();

        assertEquals("Pop Test 1",null,foo.top());
        assertEquals("Pop Size Test 1",0,foo.size());

        foo.push(i1);
        assertEquals("Push Size Test 2",1,foo.size());
        assertEquals("Push Test 2",i1,foo.top());
        foo.push(i5);
         assertEquals("Push size test 3",2,foo.size());
        assertEquals("Push test 3",i5,foo.top());
        foo.push(i10);
        assertEquals("Push Size Test 4",3,foo.size());
        assertEquals("Push Test 4",i10,foo.top());

        foo.pop();
        assertEquals("Pop Test 2",i5,foo.top());
        assertEquals("Pop Size Test 2",2,foo.size());
        foo.pop();
        assertEquals("Pop Test 3",i1,foo.top());
        assertEquals("Pop Size Test 3",1,foo.size());
        foo.pop();
        assertEquals("Pop Test 4",null,foo.top());
        assertEquals("Pop Size Test 4",0,foo.size());

        }

        //testing push
        public void testPush()
        {
        //create a stack is it empty
        Stack<Integer> foo = new Stack<Integer>();
        assertEquals("Empty Test 2",null,foo.top());
        assertEquals("Empty Size  Test 2",0,foo.size());

        foo.push(i1);
        assertEquals("Push Size Test 5",1,foo.size());
        assertEquals("Push Test 5",i1,foo.top());
        foo.push(i5);
        assertEquals("Push Size Test 6",2,foo.size());
        assertEquals("Push Test 6",i5,foo.top());
        foo.push(i10);
        assertEquals("Push Size Test 7",3,foo.size());
        assertEquals("Push Test 7",i10,foo.top());
        }

        //Tesing Size
        public void testSize()
        {
        //create a stack is it empty
        Stack<Integer> foo = new Stack<Integer>();
        assertEquals("Empty Test 3",null,foo.top());
        assertEquals("Empty Size Test 3",0,foo.size());

        foo.push(i10);
        assertEquals("Push Size Test 8",1,foo.size());
        assertEquals("Push Test 8",i10,foo.top());
        foo.push(i15);
        assertEquals("Push Size Test 9",2,foo.size());
        assertEquals("Push Test 9",i15,foo.top());
        foo.push(i20);
        assertEquals("Push size test 10",3,foo.size());
        assertEquals("Push test 10",i20,foo.top());
        foo.push(i25);
        assertEquals("Push Size Test 11",4,foo.size());
        assertEquals("Push Test 11",i25,foo.top());

        foo.pop();
        assertEquals("Pop Test 5",i20,foo.top());
        assertEquals("Pop Size Test 5",3,foo.size());
        foo.pop();
        assertEquals("Pop Test 6",i15,foo.top());
        assertEquals("Pop Size Test 6",2,foo.size());
        foo.pop();
        assertEquals("Pop Test 7",i10,foo.top());
        assertEquals("Pop Size Test 7",1,foo.size());
        foo.pop();
        assertEquals("Pop Test 8",null,foo.top());
        assertEquals("Pop Size Test 8",0,foo.size());

        }

        public void testTop()
        {
        //creat a Stack
        Stack<Integer> foo = new Stack<Integer>();
        assertEquals("Empty Test 4",null,foo.top());
        assertEquals("Empty Size Test 4",0,foo.size());

        foo.push(i1);
        assertEquals("Push Size Test 12",1,foo.size());
        assertEquals("Push Test 12",i1,foo.top());

        foo.pop();
        assertEquals("Pop Test 9",null,foo.top());
        assertEquals("Pop Size Test 9",0,foo.size());
        }




    // make sure your function begins with the string Test
    // if you want JUnit to run it.
}